package server

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/rnd-share/go-basic.com/controllers"
	"gitlab.com/rnd-share/go-basic.com/middleware"

)

func NewRouter() *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(middleware.CorsMiddleware())

	health := new(controllers.HealthController)

	router.GET("/health", health.Status)

	v1 := router.Group("v1")
	{
		userGroup := v1.Group("user")
		{
			user := new(controllers.EmployeeController)
			userGroup.GET("/", user.GetEmployee)
			userGroup.POST("/", user.CreateEmployee)
			userGroup.PUT("/:id", user.UpdateEmployee)
			userGroup.DELETE("/:id", user.DeleteEmployee)

		}
	}
	return router

}
