package models

import (
	"gorm.io/gorm"

)

type EmployeeEntity struct {
	gorm.Model `json:"model"`
	Nip  string `json:"nip" gorm:"primaryKey"`
	Name string `json:"name"`
}

