package models

import (
	"gorm.io/gorm"

)


type BiroEntity struct {
	gorm.Model
	idBiro   string `gorm:"primaryKey"`
	NamaBiro string
	Employee []EmployeeEntity
}
