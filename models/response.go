package models

type ResponseEntity struct {
	ErrorCode    string `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	OutputScheme any    `json:"output_scheme"`
}
