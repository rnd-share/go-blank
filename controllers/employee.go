package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/rnd-share/go-basic.com/db"
	"gitlab.com/rnd-share/go-basic.com/models"
	"gitlab.com/rnd-share/go-basic.com/util"

)

type EmployeeController struct{}

func (h EmployeeController) CreateEmployee(c *gin.Context) {
	var employee models.EmployeeEntity
	con := db.GetDB()
	if err := c.ShouldBindJSON(&employee); err != nil {
		c.JSON(http.StatusBadRequest, util.CreateReponse(err.Error(), "XX-01-001", err.Error()))
		return
	}
	con.Create(&employee)

	c.JSON(http.StatusCreated, util.CreateReponse("Sucess", "XX-00-000", employee))
}

func (h EmployeeController) GetEmployee(c *gin.Context) {
	con := db.GetDB()

	var employee []models.EmployeeEntity

	if err := con.Find(&employee).Error; err != nil {
		c.JSON(http.StatusBadRequest, util.CreateReponse(err.Error(), "XX-01-001", err.Error()))
		return
	}
	c.JSON(http.StatusOK, util.CreateReponse("Sucess", "XX-00-000", employee))
}

func (h EmployeeController) UpdateEmployee(c *gin.Context) {
	nip := c.Param("nip")
	con := db.GetDB()

	var employee models.EmployeeEntity
	if err := con.Where("nip = ?", nip).First(&employee).Error; err != nil {
		c.JSON(http.StatusBadRequest, util.CreateReponse(err.Error(), "XX-01-001", err.Error()))
		return
	}

	if err := c.ShouldBindJSON(&employee); err != nil {
		c.JSON(http.StatusBadRequest, util.CreateReponse(err.Error(), "XX-01-001", err.Error()))
		return
	}

	con.Save(&employee)

	c.JSON(http.StatusOK, util.CreateReponse("Sucess", "XX-00-000", employee))
}

func (h EmployeeController) DeleteEmployee(c *gin.Context) {
	nip := c.Param("nip")
	con := db.GetDB()

	var employee models.EmployeeEntity
	if err := con.Where("nip = ?", nip).First(&employee).Error; err != nil {
		c.JSON(http.StatusBadRequest, util.CreateReponse(err.Error(), "XX-01-001", err.Error()))
		return
	}

	con.Delete(&employee)

	c.JSON(http.StatusOK, util.CreateReponse("Sucess", "XX-00-000", "Delete Successfull"))
}
