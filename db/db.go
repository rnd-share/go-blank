package db

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/rnd-share/go-basic.com/models"
)

var DB *gorm.DB

func Init() error {
	dsn := "host=localhost user=postgres password=postgres dbname=postgres port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		fmt.Println(err)
		return err
	}
	DB = db

    db.AutoMigrate(&models.EmployeeEntity{})

	return nil
}

func GetDB() *gorm.DB {
	return DB
}
