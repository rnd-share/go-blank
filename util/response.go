package util

import (
	"gitlab.com/rnd-share/go-basic.com/models"

)



func  CreateReponse(message string,errorCode string,outputScheme any) models.ResponseEntity {

	var response models.ResponseEntity
	response.ErrorCode=errorCode
	response.OutputScheme=outputScheme
	response.ErrorMessage=message
	
	return response;
}