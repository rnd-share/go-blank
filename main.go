package main

import (
	"gitlab.com/rnd-share/go-basic.com/db"
	"gitlab.com/rnd-share/go-basic.com/server"
)

func main() {
	err := db.Init()
	if err != nil {
		return
	}
	server.Init()
}
